from telegram import Update, KeyboardButton, ReplyKeyboardMarkup
import requests

URL = "http://localhost:5001/book"

async def start(update: Update, context) -> None:
    
    keyboard = [
        [KeyboardButton("Lista dei comandi")],
    ]

    reply_markup = ReplyKeyboardMarkup(keyboard)

    await update.message.reply_text("Bot avviato correttamente!", reply_markup=reply_markup)

async def echo(update: Update, context) -> None:
    # Echo the user message
    msg = update.message.text

    if msg == "Lista dei comandi":
        await update.message.reply_text("Elenco di tutti i libri:\n/all\n\n\
Ricerca libro per titolo:\n/title titolo del libro\n\n\
Ricerca libri per autore:\n/author nome dell'autore\n\n\
Ricerca libri per tipo:\n/type tipo del libro\n\n\
Prenota un libro:\n/reserve titolo del libro\n\n\
Restituisci un libro:\n/release titolo del libro\n\n\
Elimina un libro:\n/delete titolo del libro\n\n\
Inserisci un nuovo libro:\n/register\ntitolo\nautore\ntipo\nnumero di copie disponibili\
")    
    else:
        await update.message.reply_text("ECHO: " + msg)

async def all(update: Update, context) -> None:
    url = URL + "/all"

    response = requests.get(url)

    if response.text == "Nessun libro trovato all'interno dell'archivio!":
        await update.message.reply_text(response.text)
    else:
        responseSlices = response.text.split("<br>")
        responseSlices.pop(len(responseSlices)-1)
        responseSlices.pop(1)

        response = responseSlices[0] + "\n\n\n"
        responseSlices.pop(0)

        for i in responseSlices:
            iSlices = i.split(":")

            title = iSlices[1]
            title = title.split(",")[0][1:]

            author = iSlices[2]
            author = author.split(",")[0][1:]

            tipo = iSlices[3]
            tipo = tipo.split(",")[0][1:]

            acp = iSlices[4]
            acp = acp.split(",")[0][1:]

            rcp = iSlices[5]
            rcp = rcp.split("}")[0][1:]

            response += "Info Book ->\nTitolo:\n" + title + "\nAutore:\n" + author + "\nTipo:\n" + tipo + "\n\
Numero copie disponibili:\n" + str(acp) + "\nNumero copie prenotate:\n" + str(rcp) + "\n\n\n"

        await update.message.reply_text(response)

async def title(update: Update, context) -> None:
    msg = update.message.text
    msg = msg.split("title ")[1]
    
    url = URL + "/title/" + msg

    response = requests.get(url)

    if response.text == "Nessun libro trovato dal titolo: " + msg.upper():
        await update.message.reply_text("Nessun libro trovato dal titolo:\n" + msg.upper())
    else:
        responseSlices = response.text.split("<br>")[2]

        iSlices = responseSlices.split(":")

        title = iSlices[1]
        title = title.split(",")[0][1:]

        author = iSlices[2]
        author = author.split(",")[0][1:]

        tipo = iSlices[3]
        tipo = tipo.split(",")[0][1:]

        acp = iSlices[4]
        acp = acp.split(",")[0][1:]

        rcp = iSlices[5]
        rcp = rcp.split("}")[0][1:]

        response = "Libro trovato:\n" + title.upper() + "\n\n\nInfo Book ->\nTitolo:\n" + title + "\n\
Autore:\n" + author + "\nTipo:\n" + tipo + "\n\
Numero copie disponibili:\n" + str(acp) + "\nNumero copie prenotate:\n" + str(rcp)

        await update.message.reply_text(response)

async def author(update: Update, context) -> None:
    msg = update.message.text
    msg = msg.split("author ")[1]

    url = URL + "/author/" + msg

    response = requests.get(url)

    if response.text == "Nessun libro trovato per l'autore: " + msg.upper():
        await update.message.reply_text("Nessun libro trovato per l'autore:\n" + msg.upper())
    else:
        responseSlices = response.text.split("<br>")

        responseSlices.pop(len(responseSlices)-1)
        responseSlices.pop(0)
        responseSlices.pop(0)

        response = "Elenco libri dell'autore:\n" + msg.upper() + "\n\n\n"

        for i in responseSlices:
            iSlices = i.split(":")

            title = iSlices[1]
            title = title.split(",")[0][1:]

            author = iSlices[2]
            author = author.split(",")[0][1:]

            tipo = iSlices[3]
            tipo = tipo.split(",")[0][1:]

            acp = iSlices[4]
            acp = acp.split(",")[0][1:]

            rcp = iSlices[5]
            rcp = rcp.split("}")[0][1:]

            response += "Info Book ->\nTitolo:\n" + title + "\nAutore:\n" + author + "\nTipo:\n" + tipo + "\n\
Numero copie disponibili:\n" + str(acp) + "\nNumero copie prenotate:\n" + str(rcp) + "\n\n\n"

        await update.message.reply_text(response)

async def tipo(update: Update, context) -> None:
    msg = update.message.text
    msg = msg.split("type ")[1]

    url = URL + "/type/" + msg

    response = requests.get(url)

    if response.text == "Nessun libro trovato per il tipo: " + msg.upper():
        await update.message.reply_text("Nessun libro trovato per il tipo:\n" + msg.upper())
    else:
        responseSlices = response.text.split("<br>")

        responseSlices.pop(len(responseSlices)-1)
        responseSlices.pop(0)
        responseSlices.pop(0)

        response = "Elenco libri di tipo:\n" + msg.upper() + "\n\n\n"

        for i in responseSlices:
            iSlices = i.split(":")

            title = iSlices[1]
            title = title.split(",")[0][1:]

            author = iSlices[2]
            author = author.split(",")[0][1:]

            tipo = iSlices[3]
            tipo = tipo.split(",")[0][1:]

            acp = iSlices[4]
            acp = acp.split(",")[0][1:]

            rcp = iSlices[5]
            rcp = rcp.split("}")[0][1:]

            response += "Info Book ->\nTitolo:\n" + title + "\nAutore:\n" + author + "\nTipo:\n" + tipo + "\n\
Numero copie disponibili:\n" + str(acp) + "\nNumero copie prenotate:\n" + str(rcp) + "\n\n\n"

        await update.message.reply_text(response)

async def reserve(update: Update, context) -> None:
    msg = update.message.text
    msg = msg.split("reserve ")[1]

    url = URL + "/reserve/" + msg

    response = requests.get(url)

    responseCheck = response.text.split("<br>")[0].split(" ")

    if responseCheck[0] == "Nessun":
        await update.message.reply_text("Nessun libro trovato dal titolo:\n" + msg.upper() + "\n\nPrenotazione non riuscita!")
    else:
        response = response.text.split("<br>")
        response = response[len(response)-1]
        
        if response == "Prenotazione non riuscita!":
            await update.message.reply_text("Libro trovato:\n" + msg.upper() + "\n\nNumero di copie non sufficienti.\
\n\nPrenotazione non riuscita!")    
        else:
            await update.message.reply_text("Libro trovato:\n" + msg.upper() + "\n\nPrenotazione riuscita!")

async def release(update: Update, context) -> None:
    msg = update.message.text
    msg = msg.split("release ")[1]

    url = URL + "/release/" + msg

    response = requests.get(url)

    responseCheck = response.text.split("<br>")[0].split(" ")

    if responseCheck[0] == "Nessun":
        await update.message.reply_text("Nessun libro trovato dal titolo:\n" + msg.upper() + "\n\nRestituzione non riuscita!")
    else:
        response = response.text.split("<br>")
        response = response[len(response)-1]
        
        if response == "Restituzione non riuscita!":
            await update.message.reply_text("Libro trovato:\n" + msg.upper() + "\n\nNessuna copia prenotata.\
\n\nRestituzione non riuscita!")    
        else:
            await update.message.reply_text("Libro trovato:\n" + msg.upper() + "\n\nRestituzione riuscita!")

async def delete(update: Update, context) -> None:
    msg = update.message.text
    msg = msg.split("delete ")[1]

    url = URL + "/delete/" + msg

    response = requests.get(url)

    responseCheck = response.text.split("<br>")[0].split(" ")

    if responseCheck[0] == "Nessun":
        await update.message.reply_text("Nessun libro trovato dal titolo:\n" + msg.upper() + "\n\nCancellazione non riuscita!")
    else:
        await update.message.reply_text("Libro trovato:\n" + msg.upper() + "\n\nCancellazione riuscita!")

async def register(update: Update, context) -> None:
    msg = update.message.text
    msg = msg.split("\n")

    title = msg[1]
    author = msg[2]
    tipo = msg[3]
    acn = int(msg[4])

    bodyRequest = {"title": title, "author": author, "type": tipo, "availableCopyNumber": acn}

    url = URL + "/register"

    response = requests.post(url, json=bodyRequest)

    await update.message.reply_text(response.text)

