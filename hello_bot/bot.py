from telegram.ext import Application, CommandHandler, MessageHandler, filters
import logging
from command import *

# Enable logging
logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
# Set higher logging level for httpx to avoid all GET and POST requests being logged
logging.getLogger("httpx").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

# Il TOKEN privato viene letto da un file di testo
tokenFile = open("token.txt", "r")
token = tokenFile.readline()
tokenFile.close()
TOKEN = token

def main() -> None:
    # Run the bot
    # Create the Application and pass it your bot's token
    application = Application.builder().token(TOKEN).build()

    application.add_handler(CommandHandler("start", start))
    
    application.add_handler(CommandHandler("all", all))
    application.add_handler(CommandHandler("title", title))
    application.add_handler(CommandHandler("author", author))
    application.add_handler(CommandHandler("type", tipo))
    application.add_handler(CommandHandler("reserve", reserve))
    application.add_handler(CommandHandler("release", release))
    application.add_handler(CommandHandler("delete", delete))
    application.add_handler(CommandHandler("register", register))

    application.add_handler(MessageHandler(filters.TEXT & ~filters.COMMAND, echo))

    # Run the bot until the user presses Ctrl-C
    application.run_polling(allowed_updates=Update.ALL_TYPES)

if __name__ == "__main__":
    main()