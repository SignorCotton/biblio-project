package com.example.hello.book;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

@Entity
public class BookEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Il parametro titolo non può essere omesso!")
    private String title;

    @NotNull(message = "Il parametro autore non può essere omesso!")
    private String author;

    @NotNull(message = "Il parametro genere non può essere omesso!")
    private String type;

    @NotNull(message = "Il numero di copie disponibili non può essere omesso!")
    private int available_copy_number;

    private int reserved_copy_number;

    // Metodi Getter

    public String getTitle() {
        return this.title;
    }

    public String getAuthor() {
        return this.author;
    }

    public String getType() {
        return this.type;
    }

    public int getAvailableCopyNumber() {
        return this.available_copy_number;
    }

    public int getReservedCopyNumber() {
        return this.reserved_copy_number;
    }

    // Metodi Setter

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAvailableCopyNumber(int available_copy_number) {
        this.available_copy_number = available_copy_number;
    }

    public void setReservedCopyNumber(int reserved_copy_number) {
        this.reserved_copy_number = reserved_copy_number;
    }

    @Override
    public String toString() {
        return "Info Book{" + "titolo: " + this.getTitle() + ", autore: " + this.getAuthor()
        + ", genere: " + this.getType() + ", numero copie disponibili: " + this.getAvailableCopyNumber()
                + ", numero copie riservate: " + this.getReservedCopyNumber() + "}";
    }
}
