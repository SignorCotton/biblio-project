package com.example.hello.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/book")
public class BookController {

    @Autowired
    BookService bookService;

    // API per la ricerca di tutti i libri presenti nell'archivio
    @GetMapping(path = "/all")
    public @ResponseBody String getAll() {
        return bookService.getAll();
    }

    // API per la ricerca di un libro in base al titolo
    @GetMapping(path = "/title/{title}")
    public @ResponseBody String getBookEntityByTitle(@PathVariable String title) {
        return bookService.getBookByTitle(title);
    }

    // API per la ricerca di libri per autore
    @GetMapping(path = "/author/{author}")
    public @ResponseBody String getBookEntityByAuthor(@PathVariable String author) {
        return bookService.getBookByAuthor(author);
    }

    // API per la ricerca di libri per tipo
    @GetMapping(path = "/type/{type}")
    public @ResponseBody String getBookEntityByType(@PathVariable String type) {
        return bookService.getBookByType(type);
    }

    // API per l'aggiunta di un nuovo libro -> il titolo di un libro è univoco
    @PostMapping(path = "/register")
    public @ResponseBody String register(@RequestBody BookDTO book) {
        return bookService.register(book);
    }

    // API per la cancellazione di un libro in base al titolo
    @GetMapping(path = "/delete/{title}")
    public @ResponseBody String delete(@PathVariable String title) {
        return bookService.delete(title);
    }

    // API per la prenotazione di un libro in base al titolo
    @GetMapping(path = "/reserve/{title}")
    public @ResponseBody String reserve(@PathVariable String title) {
        return bookService.reserve(title);
    }

    // API per la restituzione di un libro in base al titolo
    @GetMapping(path = "/release/{title}")
    public @ResponseBody String release(@PathVariable String title) {
        return bookService.release(title);
    }
}
