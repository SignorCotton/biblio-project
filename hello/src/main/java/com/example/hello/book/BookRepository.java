package com.example.hello.book;

import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<BookEntity, Integer> {
    Iterable<BookEntity> findBookEntityByTitle(String title);
    Iterable<BookEntity> findBookEntityByAuthor(String author);
    Iterable<BookEntity> findBookEntityByType(String type);
    void deleteBookEntityByTitle(String title);
}
