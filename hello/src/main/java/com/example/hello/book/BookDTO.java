package com.example.hello.book;

public class BookDTO {
    private String title;
    private String author;
    private String type;
    private int available_copy_number;

    // Costruttore
    public BookDTO(String title, String author, String type, int acp) {
        this.title = title;
        this.author = author;
        this.type = type;
        this.available_copy_number = acp;
    }

    // Metodi Getter

    public String getTitle() {
        return this.title;
    }

    public String getAuthor() {
        return this.author;
    }

    public String getType() {
        return this.type;
    }

    public int getAvailableCopyNumber() {
        return this.available_copy_number;
    }

    // Metodi Setter

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAvailableCopyNumber(int acp) {
        this.available_copy_number = acp;
    }
}
