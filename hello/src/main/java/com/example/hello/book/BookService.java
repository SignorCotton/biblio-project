package com.example.hello.book;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.StreamSupport;

@Service
@Transactional
public class BookService {

    @Autowired
    BookRepository repository;

    public BookEntity convertToEntity(BookDTO bookDTO) {
        BookEntity book = new BookEntity();

        book.setTitle(bookDTO.getTitle());
        book.setAuthor(bookDTO.getAuthor());
        book.setType(bookDTO.getType());
        book.setAvailableCopyNumber(bookDTO.getAvailableCopyNumber());

        return book;
    }

    public String register(BookDTO bookDTO) {
        Iterable<BookEntity> books = repository.findAll();

        BookEntity book = this.convertToEntity(bookDTO);

        if(book.getAvailableCopyNumber() <= 0) {
            return "Il numero di copie da salvare deve essere maggiore di 0!";
        }

        for(BookEntity b: books) {
            if(b.getTitle().equalsIgnoreCase(book.getTitle())) {
                return "Libro già inserito!";
            }
        }
        book.setReservedCopyNumber(0);
        repository.save(book);
        return "Libro inserito con successo!";
    }

    public String delete(String title) {

        Iterable<BookEntity> books = repository.findBookEntityByTitle(title);
        long size = StreamSupport.stream(books.spliterator(), false).count();
        if(size <= 0) {
            return "Nessun libro trovato dal titolo: " + title.toUpperCase() + "<br><br>Cancellazione non riuscita!";
        }
        repository.deleteBookEntityByTitle(title);
        return "Libro trovato: " + title.toUpperCase() + "<br><br>Cancellazione riuscita!";
    }

    public String reserve(String title) {

        Iterable<BookEntity> books = repository.findBookEntityByTitle(title);
        long size = StreamSupport.stream(books.spliterator(), false).count();
        if(size <= 0) {
            return "Nessun libro trovato dal titolo: " + title.toUpperCase() + "<br><br>Prenotazione non riuscita!";
        }
        BookEntity book = books.iterator().next();
        if(book.getAvailableCopyNumber() < 1) {
            return "Libro trovato: " + title.toUpperCase() + "<br>Numero di copie non sufficienti."
                    + "<br><br>Prenotazione non riuscita!";
        }
        int currentAvailableCopyNumber = book.getAvailableCopyNumber();
        int currentReservedCopyNumber = book.getReservedCopyNumber();
        book.setAvailableCopyNumber(currentAvailableCopyNumber - 1);
        book.setReservedCopyNumber(currentReservedCopyNumber + 1);
        repository.save(book);
        return "Libro trovato: " + title.toUpperCase() + "<br><br>Prenotazione riuscita!";
    }

    public String release(String title) {

        Iterable<BookEntity> books = repository.findBookEntityByTitle(title);
        long size = StreamSupport.stream(books.spliterator(), false).count();
        if(size <= 0) {
            return "Nessun libro trovato dal titolo: " + title.toUpperCase() + "<br><br>Restituzione non riuscita!";
        }
        BookEntity book = books.iterator().next();
        if(book.getReservedCopyNumber() < 1) {
            return "Libro trovato: " + title.toUpperCase() + "<br>Nessuna copia prenotata."
                    + "<br><br>Restituzione non riuscita!";
        }
        int currentAvailableCopyNumber = book.getAvailableCopyNumber();
        int currentReservedCopyNumber = book.getReservedCopyNumber();
        book.setAvailableCopyNumber(currentAvailableCopyNumber + 1);
        book.setReservedCopyNumber(currentReservedCopyNumber - 1);
        repository.save(book);
        return "Libro trovato: " + title.toUpperCase() + "<br><br>Restituzione riuscita!";
    }

    public String getAll() {

        Iterable<BookEntity> books = repository.findAll();
        long size = StreamSupport.stream(books.spliterator(), false).count();
        if(size <= 0) {
            return "Nessun libro trovato all'interno dell'archivio!";
        }
        StringBuilder textBodyResponse = new StringBuilder();
        for(BookEntity b: books) {
            textBodyResponse.append(b.toString()).append("<br>");
        }

        return "Elenco libri archivio" + "<br><br>" + textBodyResponse.substring(0);
    }

    public String getBookByTitle(String title) {

        Iterable<BookEntity> books = repository.findBookEntityByTitle(title);
        long size = StreamSupport.stream(books.spliterator(), false).count();
        if(size <= 0) {
            return "Nessun libro trovato dal titolo: " + title.toUpperCase();
        }
        StringBuilder textBodyResponse = new StringBuilder();
        for(BookEntity b: books) {
            textBodyResponse.append(b.toString()).append("<br>");
        }

        return "Libro trovato: " + title.toUpperCase() + "<br><br>" + textBodyResponse.substring(0);
    }

    public String getBookByAuthor(String author) {

        Iterable<BookEntity> books = repository.findBookEntityByAuthor(author);
        long size = StreamSupport.stream(books.spliterator(), false).count();
        if(size <= 0) {
            return "Nessun libro trovato per l'autore: " + author.toUpperCase();
        }
        StringBuilder textBodyResponse = new StringBuilder();
        for(BookEntity b: books) {
            textBodyResponse.append(b.toString()).append("<br>");
        }

        return "Elenco libri dell'autore: " + author.toUpperCase() + "<br><br>" + textBodyResponse.substring(0);
    }

    public String getBookByType(String type) {

        Iterable<BookEntity> books = repository.findBookEntityByType(type);
        long size = StreamSupport.stream(books.spliterator(), false).count();
        if(size <= 0) {
            return "Nessun libro trovato per il tipo: " + type.toUpperCase();
        }
        StringBuilder textBodyResponse = new StringBuilder();
        for(BookEntity b: books) {
            textBodyResponse.append(b.toString()).append("<br>");
        }

        return "Elenco libri di tipo: " + type.toUpperCase() + "<br><br>" + textBodyResponse.substring(0);
    }
}
