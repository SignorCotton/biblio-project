
L'esercizio realizzato prevede l'uso del framework Spring Boot e di Spring Initializr.

In particolare per il progetto sono state definite le seguenti dipendenze:
	-	Spring Web
	-	Spring Data JPA
	-	MySQL Driver
	-	Validation
	
	
All'interno della directory hello sono presenti inoltre i file:
	-	Dockerfile: per la creazione dell'immagine relativa all'applicazione Spring Boot
	-	docker-compose: per l'esecuzione dei servizi di riferimento,
		ovvero l'applicazione Spring Boot e un database MySQL
	-	hello_api.postman_collection: per il testing delle API

		
***SERVIZIO MYSQL***

Viene creato, in seguito al comando "docker compose up", un container docker
con nome mysqldb, a partire dall'immagine mysql versione 8 presente pubblicamente
all'interno del Docker Hub.
Successivamente viene effettuato il port-forwarding affinchè il servizio mysql in esecuzione
all'interno del container possa essere accessibile anche dall'esterno della rete interna
creata dal compose; in particolare viene mappata la porta 3306 del container
sulla porta 5000 dell'host in cui è in esecuzione il docker engine.

[Nota]
Tale configurazione della porta è stata poi inibita in seguito alla containerizzazione dell'applicazione stessa.

In seguito viene impostata la password di accesso al database e il nome stesso del db (bibliodb).
Viene poi definito un volume (bibliovol) per la persistenza dei dati, quando il container
viene spento e poi riavviato o qualora dovesse andare incontro a crash.

E' possibile accedere al servizio mysql all'interno del container mediante il seguente comando:
	-	docker ps: al fine di prelevare l'identificativo del container in cui esegue il servizio mysql
	-	docker exec -it "id_container" bash
		Si aprirà un terminale del tipo: bash-5.1#
		in cui sarà possibile richiamare il servizio mysql, con il seguente comando:
			-	mysql -u root -p
				una volta inserita la password (root) verrà mostrato il monitor MySQL.
				Viene poi selezionato il rispettivo database con il seguente comando:
					-	use bibliodb;
						Infine sarà possibile eseguire le query in maniera standard per visualizzare i dati:
							-	select * from book_entiy;
						E' possibile chiudere il monitor MySQL con il seguente comando:
							-	quit
								dunque si ritorna all'interno del container,
								avendo eseguito in principio il comando bash
						E' possibile terminare il processo bash con il seguente comando:
							-	exit
Il controllo viene dunque ritornato al terminale che ha eseuito il comando docker exec.


***SERVIZIO APPLICATIVO***

All'interno della directory book (/hello/src/main/java/com/example/hello/book)
troviamo i seguenti file organizzati secondo struttura byKind:
	-	BookController
	-	BookDTO
	-	BookEntity
	-	BookRepository
	-	BookService
	
BookEntity

Viene definita la classe BookEntity, che mediante il tool ORM viene poi mappata
nella tabella book_entity del database bibliodb.
Vengono definiti i seguenti attributi:
	-	title: indica il titolo di un libro
	-	author: indica l'autore di un libro
	-	type: indica il tipo/genere di un libro
	-	available_copy_number: indica il numero di copie disponibili di un libro
	-	reserved_copy_number: indica il numero di copie prenotate di un libro
	
BookDTO

Viene definita la classe BookDTO, per la comunicazione tra diversi livelli ove necessario,
in particolare tra il livello Controller e il livello Service

BookRepository

Viene definita l'interfaccia BookRepository che estende CrudRepository, con l'obiettivo di
dichiarare differenti metodi per le operazioni CRUD, alcuni dei quali già presenti di default, come:
	-	save: utilizzato sia per operazioni di Create, sia di Update
	-	findAll: per tornare l'intero elenco di dati memorizzati
Vengono definiti i seguenti metodi:
	-	findBookEntityByTitle: per la ricerca di un libro datone il titolo
	-	findBookEntityByAuthor: per la ricerca di libri datone l'autore
	-	findBookEntityByType: per la ricerca di libri datone il tipo
	-	deleteBookEntityByTitle: per la cancellazione di un libro datone il titolo
	
BookService

Viene definita la classe BookService, che offre i propri servizi al livello Controller, mentre
utilizza i servizi offerti dal livello Repository in merito allo storage dei dati.
Vengono definiti i seguenti metodi:
	-	convertToEntity: per effettuare la conversione di un oggetto di tipo BookDTO
		in un oggetto di tipo BookEntity
	-	getAll: per visualizzare l'elenco di tutti i libri presenti nell'archivio
	-	getBookByTitle:	per visualizzare un libro datone il titolo
	-	getBookByAuthor: per visualizzare i libri datone l'autore
	-	getBookByType: per visualizzare i libri datone il tipo/genere
	-	register: per memorizzare un nuovo libro all'interno dell'archivio,
		qualora tale titolo non fosse già presente
	-	delete: per cancellare un libro datone il titolo
	-	reserve: per prenotare un libro datone il titolo,
		qualora vi fossero ancora copie disponibili
	-	release: per restituire un libro datone il titolo
	
BookController

Viene definita la classe BookController, che definisce quali sono le API esposte all'esterno,
che posso essere chiamate da un client, e come ciascuna chiamata deve essere gestita internamente.
Vengono implementate le seguenti API:
	[Nota]: il suddetto Controller definisce tale path condiviso tra le varie API
		-	http://localhost:8080/book
			[Nota]: in fase di containerizzazione dell'applicazione è stato effettuato
			il seguente port-forwarding (5001:8080), dunque il container dell'applicazione
			risulterà essere in ascolto sulla porta 5001 dell'host che esegue il docker engine
			-	/all
				API GET -> per la ricerca di tutti i libri presenti nell'archivio
			-	/title/{title}
				API GET -> per la ricerca di un libro datone il titolo
			-	/author/{author}
				API GET -> per la ricerca di libri datone l'autore
			-	/type/{type}
				API GET -> per la ricerca di libri datone il tipo/genere
			-	/register
				API POST -> per l'inserimento di un nuovo libro, considerandone il titolo univoco
				Il body della richiesta deve essere trasmesso nella seguente forma JSON:
					-	{"title": "myTitle", "author": "myAuthor", "type": "myType", "availableCopyNumber": 10}
			-	/delete/{title}
				API GET -> per la cancellazione di un libro datone il titolo
			-	/reserve/{title}
				API GET -> per la prenotazione di un libro datone il titolo,
				qualora vi fossero ancora copie disponibili
			-	/release/{title}
				API GET -> per la restituzione di un libro datone il titolo

				
***ACCESSO ALLE API***

Per testare e interrogare le API implementate è stato utilizzato un semplice browser e il tool Postman,
in particolare per l'esecuzione della funzionalità di /register, in quanto previsto il body della richiesta
con i dati precisamente formattati (come visto precedentemente).

Utilizzando il tool Postman è possibile testare le suddette API andando ad importare la postman-collection
di nome hello_api, situata all'interno della directory hello.

[Nota]
E' possibile anche interrogare le suddette API mediante l'utilizzo di un bot Telegram,
andando ad utilizzare tutta una lista di comandi definiti e integrati appositamente.
Tale ulteriore servizio client, situato all'interno della directory hello_bot,
è stato sviluppato in linguaggio Python, dunque per l'esecuzione necessita di relativo interprete.
Aprendo un terminale (in Windows), a partire dalla directory hello_bot, sarà necessario
attivare l'ambiente virtuale mediante il comando:
	-	./venv/Scripts/activate
	Per eseguire lo script del bot basterà dunque il seguente comando:
	-	python bot.py
	
	
***COLLEGAMENTO CON IL SERVIZIO MYSQL***

Viene effettuato utilizzando le dipendenze Sping Data JPA e MySQL Driver.
Viene dunque definita la specifica configurazine all'interno del file application.properties,
dove principalmente si osserva il collegamento con il database secondo l'indirizzo url del servizio:
	-	spring.datasource.url=jdbc:mysql://mysqldb:3306/bibliodb?serverTimezone=UTC
		In particolare si notino i seguenti parametri:
			-	mysqldb: specifica l'indirizzo IP dell'host/nome del container in cui è in esecuzione il servizio MySQL
			-	3306: specifica la porta su cui è in ascolto il servizio MySQL (inibito il port-forwarding)
			-	bibiodb: specifica il nome del database a cui collegarsi
	
	
***CREAZIONE DELL'APPLICATIVO JAR CON MAVEN***

Per la pacchettizzazione e generazione dell'applicativo, utilizzando la versione di Java 22 e
la versione di Apache Maven 3.9.6, vengono utilizzati i seguenti comandi:
	-	mvn clean
	-	mvn clean package
	All'interno della directory target verranno dunque generati i file:
	-	hello-0.0.1-SNAPSHOT.jar
	-	hello-0.0.1-SNAPSHOT.jar.original
	
	
***CONTAINERIZZAZIONE APPLICATIVO***

Viene eseguita mediante apposita definizione del rispettivo Dockerfile,
dove osserviamo le seguenti configurazioni principali:
	-	FROM openjdk:22-jdk-slim: permette di definire lo strato dell'infrastruttura di base del container,
		definendo come sistema operativo Slim e installare la openjdk versione 22, ovvero quella utilizzata
	-	ENTRYPOINT ["java", "-jar", "biblio.jar"]: corrisponde al punto di start del container,
		ovvero il primo comando che viene eseguito, una volta costruiti gli strati sottostanti
		
All'interno del docker-compose vengono dunque definite anche le proprietà del servizio applicativo,
che come già detto sarà in ascolto sulla porta 5001 all'indirizzo dell'host (localhost).

Di seguito il comando per avviare/stoppare i vari servizi mediante docker compose:
	-	avvio: docker compose up --build
	-	stop: docker compose down
	

***ACCESSO AL REPOSITORY SU DOCKER HUB***

Le immagini docker utilizzate inoltre sono state pushate presso il repository pubblico
accessibile da Docker Hub al seguente indirizzo:
	-	salvoscan/bibliorepo